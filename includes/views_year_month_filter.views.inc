<?php
/**
 * @file
 * Views hooks for views_year_month_filter.
 */

/**
 * Implements hook_views_data().
 */
function views_year_month_filter_views_data() {
  $data = array();

  $tables = date_views_base_tables();

  foreach ($tables as $base_table => $entity) {
    // The flexible date filter.
    $data[$base_table]['year_month_date_filter'] = array(
      'group' => t('Date'),
      'title' => t('Year-month (!base_table)', array('!base_table' => $base_table)),
      'help' => t('Filter by year and month for Views !base_table date fields.', array('!base_table' => $base_table)),
      'filter' => array(
        'handler' => 'views_year_month_filter_handler',
        'empty field name' => t('Undated'),
        'is date' => TRUE,
      ),
    );
  }

  return $data;
}
