<?php
/**
 * @file
 * A filter for year month, but exposed in a single field.
 */

class views_year_month_filter_handler extends date_views_filter_handler_simple {
  function option_definition() {
    $options = parent::option_definition();
    $options['date_field'] = array('default' => NULL);
    $options['view_reference'] = array('default' => NULL);
    return $options;
  }

  function extra_options_form(&$form, &$form_state) {
    parent::extra_options_form($form, $form_state);

    if (isset($form['granularity'])) {
      $form['granularity']['#type'] = 'hidden';
      unset($form['granularity']['#options']);
      $form['granularity']['#value'] = 'month';
      $form['granularity']['#_default_value'] = 'month';
      
    }
    $fields = date_views_fields($this->base_table);
    $options = array();
    foreach ($fields['name'] as $name => $field) {
      if ($field['field_name'] != 'year_month_date_filter') {
        $options[$name] = $field['label'];
      }
    }

    $form['date_field'] = array(
      '#title' => t('Date field'),
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $this->options['date_field'],
      '#description' => t('Select date field.'),
      '#required' => TRUE,
    );

    $enabled_views = array(
      '' => t('-- None --'),
    );
    $views = views_get_all_views();
    ksort($views);
    foreach ($views as $view) {
      if (!($view->name == $this->view->name)) {
        // Add to the list except current view.
        if (!isset($views[$view->name]->disabled) || !$views[$view->name]->disabled) {
          $enabled_views[$view->name] = check_plain($view->name);
          $displays = array();
          foreach ($view->display as $id => $display) {
            $displays[$view->name . ":" . $id] = check_plain("-- {$display->display_title}");
          }
        }
        $enabled_views += $displays;
      }
    }
    $form['view_reference'] = array(
      '#title' => t('View for date options'),
      '#type' => 'select',
      '#options' => $enabled_views,
      '#default_value' => $this->options['view_reference'],
      '#description' => t('Select a view for the date options.'),
      '#required' => FALSE,
    );

  }

  function operators() {
    $operators = array(
      '=' => array(
        'title' => t('Is in year-month'),
        'method' => 'op_year_month',
        'short' => t('year_month'),
        'values' => 1,
      ),
      '<>' => array(
        'title' => t('Is NOT in year-month'),
        'method' => 'op_year_month',
        'short' => t('year_month'),
        'values' => 1,
      ),
    );
    return $operators;
  }

  function op_year_month($field) {
    $this->get_query_fields();
    if (empty($this->query_fields)) {
      return;
    }
    $field_names = array();
    foreach ((array) $this->query_fields as $query_field) {
      $field = $query_field['field'];
      $this->date_handler = $query_field['date_handler'];
      // Respect relationships when determining the table alias.
      if ($field['table_name'] != $this->table || !empty($this->relationship)) {
        $this->related_table_alias = $this->query->ensure_table($field['table_name'], $this->relationship);
      }
      $table_alias = !empty($this->related_table_alias) ? $this->related_table_alias : $field['table_name'];

      // TODO: Research adding delta field code from parent:op_simple here.
      $field_name = $table_alias . '.' . $field['field_name'];
      $field_names[] = $field_name;
    }
    if (count($field_names) == 1) {
      $value = $this->get_filter_value('value', $this->value['value']);
      $comp_date = new DateObject($value, date_default_timezone(), $this->format);

      $date_field = $this->date_handler->sql_field($field_names[0], NULL, $comp_date);
      $date_field = $this->date_handler->sql_format($this->format, $date_field);
      $placeholder = $this->placeholder();
      $group = !empty($this->options['date_group']) ? $this->options['date_group'] : $this->options['group'];
      if ($this->operator == '=') {
        $this->query->add_where_expression($group, "$date_field = $placeholder", array($placeholder => $value));
      }
      else {
        $this->query->add_where_expression($group, "$date_field != $placeholder", array($placeholder => $value));
      }
    }
  }

  // Update the summary values to provide meaningful information.
  function admin_summary() {
    if ($this->options['exposed']) {
      return t('exposed');
    }
    $value = check_plain(!empty($this->options['default_date']) ? $this->options['default_date'] : $this->options['value']['value']);
    $op = $this->operator;
    return t('@op @value', array(
      '@op' => $op,
      '@value' => $value,
    ));
  }

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    if (($this->options['form_type'] == 'date_select') && (isset($form['value']['value']))) {
      $form['value']['value']['#type'] = 'select';
      $form['value']['value']['#date_format'] = 'Y-m';
      unset($form['value']['value']['#process']);
      unset($form['value']['value']['#size']);
      unset($form['value']['#element_validate']);
      $form['value']['value']['#options'] = $this->value_options();
      $value = check_plain(!empty($this->options['default_date']) ? $this->options['default_date'] : $this->options['value']['value']);
      $form['value']['value']['#default_value'] = $value;
    }
  }

  function value_options() {
    if ($this->options['view_reference']) {
      return $this->value_options_view_reference();
    }
    return $this->value_options_default();
  }

  function value_options_view_reference() {
    $ref_view = $this->options['view_reference'];
    $view_parts = explode(':', $ref_view);
    $view_name = $view_parts[0];
    $display_id = NULL;
    if (!empty($view_parts[1])) {
      $display_id = $view_parts[1];
    }
    $view = views_get_view($view_name);
    $view->set_arguments($this->view->args);
    if (is_string($display_id)) {
      $view->set_display($display_id);
    }
    $view->pre_execute();
    $view->execute();
    $result_field = NULL;
    foreach ($view->field as $field_name => $field) {
      if ($field->field_info['type'] == 'datetime') {
        // Get first date field.
        $result_field = $field;
        break;
      }
    }

    $options = array(
      '' => t('All'),
    );
    if (!empty($result_field)) {
      $result_field_key = 'field_' . $result_field->field_info['field_name'];
      foreach ($view->result as $rod_id => $row) {
        $row_data = (array) $row;
        if (!empty($row_data[$result_field_key][0]['raw']['value'])) {
          $date = strtotime($row_data[$result_field_key][0]['raw']['value']);
          $item = format_date($date, 'custom', 'Y-m');
          $label = format_date($date, 'custom', 'F, Y');
          if (!isset($options[$item])) {
            $options[$item] = $label;
          }
        }
      }
    }
    return $options;
  }

  function value_options_default() {
    $this->get_query_fields();
    if (empty($this->query_fields)) {
      return;
    }
    $query = clone $this->query;
    $query->view = $this->view;
    $query_field = $this->query_fields[0];
    $field = $query_field['field'];
    $this->date_handler = $query_field['date_handler'];
    // Respect relationships when determining the table alias.
    if ($field['table_name'] != $this->table || !empty($this->relationship)) {
      $this->related_table_alias = $query->ensure_table($field['table_name'], $this->relationship);
    }
    $table_alias = !empty($this->related_table_alias) ? $this->related_table_alias : $field['table_name'];
    $field_name = $table_alias . '.' . $field['field_name'];
    $field_names[] = $field_name;

    $year_month = array();
    if (count($field_names) == 1) {
      // Get the possible options from table.
      $value = $this->get_filter_value('value', $this->value['value']);
      $comp_date = new DateObject($value, date_default_timezone(), $this->format);
      $date_field = $this->date_handler->sql_field($field_names[0], NULL, $comp_date);
      $date_field = $this->date_handler->sql_format($this->format, $date_field);
      $placeholder = $this->placeholder();
      $group = !empty($this->options['date_group']) ? $this->options['date_group'] : $this->options['group'];
      $query->add_where_expression($group, "$date_field IS NOT NULL");
      $db_query = $query->query(FALSE);
      $db_query->addExpression($date_field, '');
      $db_query->groupBy($date_field);
      $db_query->orderBy($date_field, 'DESC');

      foreach ($db_query->execute() as $row) {
        $year_month[] = (string) $row->expression;
      }
    }
    $options = array(
      '' => t('All'),
    );
    foreach ($year_month as $item) {
      list($year, $month) = explode('-', $item);
      $label = format_date(mktime(0, 0, 0, $month, 1, $year), 'custom', 'F, Y');
      $options[$item] = $label;
    }
    return $options;
  }


  function get_query_fields() {
    $fields = date_views_fields($this->base_table);
    $fields = $fields['name'];
    $this->query_fields = array();
    $date_field = $this->options['date_field'];

    if (array_key_exists($date_field, $fields) && $field = $fields[$date_field]) {
      $date_handler = new date_sql_handler($field['sql_type'], date_default_timezone());
      $date_handler->granularity = $this->options['granularity'];
      $date_handler->db_timezone = date_get_timezone_db($field['tz_handling']);
      $date_handler->local_timezone = date_get_timezone($field['tz_handling']);
      $this->query_fields[] = array(
        'field' => $field,
        'date_handler' => $date_handler,
      );
    }
  }

}
